package org.grakovne.mds.uploader;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.File;

public class Uploader {

    public HttpResponse<String> upload(File file) {

        try {
            return Unirest.post("http://grakovne.org:5050/api/v1/story")
                .header("Authorization", "Basic Z3Jha292bmU6cmVkaDByc2U=")
                .field("audio", file)
                .asString();
        } catch (UnirestException ex) {
            throw new RuntimeException(ex);
        }
    }
}
