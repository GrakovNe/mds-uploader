package org.grakovne.mds.uploader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FileUtils {

    private File doneFiles;
    private File sourceFiles;
    private File errorFiles;
    private File rootDirectory;

    public FileUtils() {
        rootDirectory = new File("data");
        doneFiles = new File(rootDirectory, "done");
        sourceFiles = new File(rootDirectory, "source");
        errorFiles = new File(rootDirectory, "error");
    }

    public void prepareDirectoryTree() {
        if (rootDirectory.isDirectory()) {
            return;
        }

        rootDirectory.mkdir();
        doneFiles.mkdir();
        sourceFiles.mkdir();
        errorFiles.mkdir();
    }

    public List<File> getSourceFiles() {
        return Arrays.stream(sourceFiles.listFiles())
            .filter(file -> file.getName().endsWith(".mp3"))
            .collect(Collectors.toList());
    }

    public void markFileDone(File file) {
        try {
            Files.move(file.toPath(), new File(doneFiles, file.getName()).toPath());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void markFileError(File file) {
        try {
            Files.move(file.toPath(), new File(errorFiles, file.getName()).toPath());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
