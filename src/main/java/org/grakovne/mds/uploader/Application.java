package org.grakovne.mds.uploader;

public class Application {

    public static void main(String[] args) {

        FileUtils utils = new FileUtils();
        utils.prepareDirectoryTree();
        utils.getSourceFiles().parallelStream().forEach(file -> {
            int status;
            try {
                System.out.println("Processing File: " + file.getAbsolutePath());
                status = new Uploader().upload(file).getStatus();
            } catch (Exception ex) {
                status = 500;
            }

            if (status != 200) {
                System.out.println("Can't upload file: " + file.getAbsolutePath());
                new FileUtils().markFileError(file);
            } else {
                System.out.println("File uploaded successfully: " + file.getAbsolutePath());
                new FileUtils().markFileDone(file);
            }
        });
    }
}
